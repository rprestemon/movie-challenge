# Movie Challenge
To run:

```bash
git clone https://gitlab.com/rprestemon/movie-challenge.git 
cd movie-challenge
cp .env.example .env.local
```

Add your MovieDB API key where appropriate in `.env.local` then run `npm start` 

### Summary
This was fun. I feel like I have a workable MVP but it needs refinement. 
Since I've only been working with React for about a week I spent a little more time on some concepts than I would have liked.  
I have a lot to learn and have started reading about component life cycle, optimization, and testing. 

I will probably revisit this repository as a learning tool over the next few days! 
Had I spent a few more hours learning, I feel I could I have flushed out the issues. However, I didn't think that was in the spirit of a coding challenge.

#### For additional Libraries I used:
1. [axios](https://www.npmjs.com/package/axios)
2. [lodash debounce](https://www.npmjs.com/package/lodash.debounce)
3. [styled-components](https://www.npmjs.com/package/styled-components)


#### Takeaways for me: 
1. Since I'm new to React I probably spent more time on some basics than I would have liked such as learning about the async nature of `setState`.
2. I ran into some issues with testing
    * Overall I need to learn more about frontend testing. This is an admittedly new area for me. It has become a focus alongside learning React.
    * I think better component design and understanding of component life cycle will help in writing more testable code
3. I want to read more about component architecture and organization.

#### Next Steps:
1. I want to clean up and abstract some of my styled components into more reusable pieces.
2. I feel like I have too much in App.js right now and want to find a better way to organize my code there. 
3. Better understand testing patterns in Jest and React. Write appropriate tests for core functionality.


# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)

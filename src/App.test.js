import { act, render } from '@testing-library/react'
import App from './App';

test('renders learn react link', () => {
  render(<App />);

  const onChange = jest.fn();
  const input = document.querySelector("input[type='text']");
  const title = document.querySelector('h1');

  expect(title.textContent).toBe('Movie Search App');
  expect(input).toBeDefined();

  // This was a failed attempt at user input testing based on React docs
  // I'm guessing this might have to do with my component boot strapping

  //const keyboardEvent = new KeyboardEvent("keyup", { 'key': 'C'});
  //act(() => input.dispatchEvent(keyboardEvent));
  //expect(input.getAttribute('value')).toBe("C");
  // expect(onChange).toHaveBeenCalledTimes(1);

  // const button = document.querySelector("button");
  // expect(button.innerHTML).toBe("Turn on");
});

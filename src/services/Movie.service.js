const axios = require("axios");

export default class MovieService {
  static BASE_URL = 'https://api.themoviedb.org/3/search/movie'

  static async searchByTitle (query, page) {
    return await axios.get(this.BASE_URL, {
      params: {
        'api_key': process.env.REACT_APP_MOVIE_KEY,
        'query': query,
        'page': page,
        'language': 'en-US',
      }
    });
  };
}

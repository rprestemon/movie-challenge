import { render, unmountComponentAtNode } from 'react-dom'
import MovieCard from '../movie-card/MovieCard'
import MovieList from './MovieList'

let container = null;

beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

const movies = [
  {
    title: "Thor",
    release_date:"2000-04-17",
    overview: "hammer man make sky go zap",
    backdrop_path: "path_to_image.png",
    vote_average: 7.75,
    vote_count: 70000,
    id: 1,
  },
  {
    title: "Captain Underpants",
    release_date:"2010-05-21",
    overview: "apparently about a kid in underpants",
    backdrop_path: "path_to_image.png",
    vote_average: 10,
    vote_count: 100000,
    id: 2,
  }
];

test("should render a list of movies", () => {
  render(<MovieList movies={movies} />, container);
  const listItems = container.querySelector(".movie-list").children;

  expect(listItems.length).toBe(2);
  expect(listItems[0].getAttribute('id')).toBe('movie_1');
});

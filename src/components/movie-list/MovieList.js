import MovieCard from '../movie-card/MovieCard'
import styled from 'styled-components'

const ResultList = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  margin: 1em auto;
`
const MovieWrapper = styled.div`
  margin: 1em auto;
  flex: 1 0 100%;
  
  @media (min-width: 800px) {
    flex: 0 1 calc(45% - .5em);
  }

  @media (min-width: 1280px) {
    flex: 0 1 calc(31% - .5em);
  }
`

export default function MovieList(props) {

  const movies = (props.movies || [])
    .filter((movie) => {
      return movie.release_date !== undefined && movie.release_date.length > 0
    })
    .map((movie) => {
      return (
        <MovieWrapper key={movie.id + '_wrapper'} id={'movie_'+movie.id}>
          <MovieCard key={movie.id} movie={movie} />
        </MovieWrapper>
      )
  });

  return (
    <ResultList className="movie-list">
      {movies}
    </ResultList>
  )
}

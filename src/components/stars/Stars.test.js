import { render, screen } from '@testing-library/react'
import Stars from './Stars'

test('renders stars component', () => {
  render(<Stars rating={3} total={5} />);
  const stars = document.querySelector('div');
  expect(stars).toBeInTheDocument();
});

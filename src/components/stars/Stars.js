import styled from 'styled-components'

const StarReview = styled.div`
  
  --percent: calc(${props => props.rating} / ${props => props.total} * 100%);
  display: inline-block;
  font-size: 14px;
  line-height: 1;

  ::before {
    content: '★★★★★';
    letter-spacing: 3px;
    background: linear-gradient(90deg, #fc0 var(--percent), #d0d0d0 var(--percent));
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
  }
`;

export default function Stars(props) {
  return (
    <StarReview
      className='stars'
      rating={props.rating}
      total={props.total}
      aria-label={`Rating of this movie is ${props.rating} out of ${props.total}.`}
    />
  )
}

import { render, unmountComponentAtNode } from 'react-dom'
import MovieCard from './MovieCard'

let container = null;

beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

const movie = {
  title: "Thor",
  release_date:"2000-04-17",
  overview: "hammer man make sky go zap",
  poster_path: "path_to_image.png",
  vote_average: 7.75,
  vote_count: 70000,
};

test("renders movie card", async () => {

  render(<MovieCard movie={movie} />, container);
  expect(container.querySelector("h3").textContent).toBe(movie.title);
  expect(container.querySelector("p").textContent).toBe(movie.overview);

  // I read that re-rendering the component will run the hooks.
  // It seems to check out as far as I can tell. Interested in a better set up.
  render(<MovieCard movie={movie} />, container);

  const timeElement = container.querySelector("time");
  expect(timeElement.textContent).toBe('2000');
  expect(timeElement.getAttribute('datetime')).toBe(movie.release_date);
});



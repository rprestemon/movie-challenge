import styled from 'styled-components'

export const CardContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  max-width: 95%;
  margin: 0 auto;
  justify-content: space-evenly;
  box-shadow: 0 1px 8px black;
  background: #fff;
`

export const MovieImage = styled.div`
  flex: 0 1 33%;
  width: 31%;
  background: no-repeat url(${props => props.imageUrl});
  background-position: center;
  background-size: cover;
  
  @media (min-width: 600px) {
    width: 100%;
  }
`

export const MovieDescription = styled.p`
  font-size: .8em;
  width: 95%;
`

export const MovieMetadata = styled.div`
  flex: 1 0 50%;
  width: 50%;
  min-height: 280px;
  margin: 0 1em;
  
  .year {
    color: grey;
    font-size: .8em;
  }
`;

export const MovieReviewCount = styled.span`
  font-size: .8em;
  text-align: right;
`;

export const MovieTitle = styled.h3`
  font-size: 1.1em;
  margin: .3em 0;
`

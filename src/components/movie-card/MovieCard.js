import {useEffect} from 'react';

import Stars from '../stars/Stars'
import {
  CardContainer,
  MovieDescription,
  MovieMetadata,
  MovieTitle,
  MovieImage,
  MovieReviewCount
} from './MovieCard.styles'
import noimage from '../../assets/image-not-available.png'

export default function MovieCard(props) {

  // Handle some basic data transformation for the movie
  useEffect(() => {
    const movie = props.movie;
    // get release year only from the existing date string
    movie.release_year = movie.release_date.split('-')[0];

    // truncate description text with little elegance
    if(movie.overview.length > 60 ) {
      movie.overview = movie.overview.substring(0, 200) + '...';
    }

    if (movie.backdrop_path) {
      movie.preview_image = `https://image.tmdb.org/t/p/original/${movie.poster_path}`;
    } else {
      movie.preview_image = noimage;
    }
  }, []);

  return (
    <CardContainer>
      <MovieImage imageUrl={props.movie.preview_image}  />
      <MovieMetadata>
        <MovieTitle>{props.movie.title}</MovieTitle>
        <time className="year" dateTime={props.movie.release_date}>{props.movie.release_year}</time>

        <MovieDescription>
          {props.movie.overview}
        </MovieDescription>

        {
          props.movie.vote_count > 0 ?
          <div>
            <Stars rating={props.movie.vote_average / 2} total={5} />
            <MovieReviewCount>{props.movie.vote_count} Reviews</MovieReviewCount>
          </div>
          : <div><MovieReviewCount>No Reviews Yet</MovieReviewCount></div>
        }

      </MovieMetadata>
    </CardContainer>
  )
}

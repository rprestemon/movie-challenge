import React, {useState, useCallback, useEffect} from 'react';
import { CenteredContainer, CTAButton, FooterContainer, Header, SearchInput, Spinner } from './App.styles'
import MovieList from './components/movie-list/MovieList'
import MovieService from './services/Movie.service'
import logo from './assets/logo.svg';

const debounce = require("lodash.debounce");

function App() {
  // use query from query string if provided
  let queryParams = new URLSearchParams(window.location.search);
  const [query, setQuery] = useState(queryParams.get('query'));

  const [movies, setMovies] = useState([]);
  const [page, setPage] = useState(0);
  const [totalPages, setTotalPages] = useState(0);
  const [loading, setLoading] = useState(false);


  // Load data from provided query params
  useEffect(() => {
    if (query && query.length > 0) {
      search(query);

      // update query string for sharing
      queryParams.set('query', query);
      window.history.replaceState(null, null, `?${queryParams.toString()}`);
    }
  }, [query]);

  // I'll be honest, I'm still just starting to understand useCallback.
  // I found this when searching about debouncing in React
  const search = useCallback(
    // use lodash debounce to prevent too many API requests
    debounce(
async (searchQuery) => {
        setLoading(true);

        const response = await MovieService.searchByTitle(searchQuery);
        setMovies(response.data.results);
        setPage(response.data.page);
        setTotalPages(response.data.total_pages);

        setLoading(false);
      }, 200
    ),
    []
  );

  // User interaction

  const submitSearch = async (e) => {
    setQuery(e.target.value);
  }

  const loadMore = async (e) => {
    e.preventDefault();
    const response = await MovieService.searchByTitle(query, page + 1);
    setLoading(true);

    // Ran into an edge case where the API was returning duplicates on new pages so I check for
    // existing ids in the current set of movies
    const filteredMovies = response.data.results.filter((newMovie) => {
      const existingMovie = movies.find((existingMovie) => existingMovie.id === newMovie.id);
      return existingMovie === undefined;
    });

    setMovies(movies.concat(filteredMovies));
    setPage(response.data.page);
    setTotalPages(response.data.total_pages);

    setLoading(false);
  }

  return (
      <div>
        <Header>
          <h1>Movie Search App</h1>
          <SearchInput
            type="text"
            onChange={submitSearch}
            value={query || ''}
            placeholder="Start typing to search"
          />
        </Header>

        <MovieList movies={movies} />

        { page > 0 && page !== totalPages && movies.length > 0 &&
          <FooterContainer containerWidth="100%">
            <p>Showing {page} of {totalPages} Pages</p>
            <CTAButton onClick={loadMore}>Load More</CTAButton>
          </FooterContainer>
        }

        <Spinner src={logo} alt="logo" show={loading} />
      </div>
  );
}

export default App;

import styled, { keyframes } from 'styled-components'

export const CenteredContainer = styled.div`
  width: ${props => props.containerWidth};
  margin: 0 auto;
  text-align: center;
`

export const FooterContainer = styled(CenteredContainer)`
  margin: 1em auto;
  box-shadow: 1px -3px 3px rgba(200, 200, 200, 1);
  padding-top: .5em;
  
`

export const CTAButton = styled.button`
  padding: 1em;
  font-size: 1.1em;
  background-color: #24c4f8;
  border: 1px solid #24c4f8;
  color: #fff;
  cursor: pointer;
`

export const SearchInput = styled.input`
  text-align: center;
  width: 80%;
  line-height: 2em;
  font-size: 1.5em;
`

export const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`

export const Spinner = styled.img`
  height: 40vmin;
  width: 40vmin;
  pointer-events: none;
  position: fixed;
  top: 30%;
  left: 35%;
  display: ${props => props.show ? 'block' : 'none'};

  animation: ${rotate} 1s linear infinite;
`

export const Header = styled.header`
  text-align: center;
  padding-bottom: 1.1em;
  box-shadow: 2px 5px 3px rgba(200, 200, 200, 1);
  background-color: rgba(36, 196, 248, 0.3);

  h1 {
    margin-top: 0;
    padding-top: 1em;
  }
`
